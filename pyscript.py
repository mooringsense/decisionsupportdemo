from js import localStorage

def sub(*args,**kwargs):

    #Set localStorage
    localStorage.setItem("Maintenancethreshold",Element('maintenancethreshold').value)
    localStorage.setItem("Duration",Element('duration').value)

    #check radio button
    if document.getElementById("nostrategy").checked:
        Mstrategy = "repair"
        Mstrategyvalue = document.getElementById("nostrategy").value
    if document.getElementById("onlysummer").checked:
        Mstrategy = "summer"
        Mstrategyvalue = document.getElementById("onlysummer").value
    if document.getElementById("period").checked:
        Mstrategy = "period"
        Mstrategyvalue = document.getElementById("period").value

    localStorage.setItem("Maintenancestrategy", Mstrategyvalue)

    result_place = Element('parametermessage')
    result_place.write(f"Maintenance will be done at {Element('maintenancethreshold').value} component health. The repair strategy chosen is: '' {Mstrategyvalue} ''  and the simulation will run for 20 years")

    #Choose scenario
    if localStorage.getItem("Maintenancethreshold") == "10%":

        if Mstrategy == "repair":
            Scenario = 1
        elif Mstrategy == "summer":
            Scenario = 2
        elif Mstrategy == "period":
            Scenario = 3
    elif localStorage.getItem("Maintenancethreshold") == "30%":

        if Mstrategy == "repair":
            Scenario = 4
        elif Mstrategy == "summer":
            Scenario = 5
        elif Mstrategy == "period":
            Scenario = 6

    localStorage.setItem("Scenario", Scenario) #set scenario in localstorage
    document.getElementById("workordergenerator").style.visibility="visible"

    #Make the other block invisble again if they are visible
    document.getElementById("componenthealth").style.visibility="hidden"
    document.getElementById("workorderlistblock").style.visibility="hidden"
    document.getElementById("resultblock").style.visibility="hidden"

def loadcomponenthealth(*args,**kwargs):
    windturbine = Element("windturbine").value
    Scenario = localStorage.getItem("Scenario")
    document.getElementById("chainCHimage").src = "./figures/" + Scenario + "/health_T" + windturbine + "_Chain_20years.png"
    document.getElementById("connectorCHimage").src = "./figures/" + Scenario + "/health_T" + windturbine + "_Connectors_20years.png"
    document.getElementById("componenthealth").style.visibility="visible"

def Generateworkorders(*args,**kwargs):
    Scenario = localStorage.getItem("Scenario")
    document.getElementById("csvtable").src = "./scenario" + Scenario + ".html"
    document.getElementById("workorderlistblock").style.visibility="visible"



def show(*args,**kwargs):
    Scenario = localStorage.getItem("Scenario")
    document.getElementById("results").src = "./resutlscenario" + Scenario + ".html"
    document.getElementById("resultblock").style.visibility="visible"